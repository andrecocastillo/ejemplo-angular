import { Component, Output, Input } from '@angular/core';
import { DatosinicialesService } from './service/datosiniciales.service';
import { LocalstorageService } from './provider/localstorage.service';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'todo';

  
  constructor(private datosini:DatosinicialesService, private localSt:LocalstorageService) {
    
        // Validar si el archivo se encuentra en local storage
        let archivoCargado = this.localSt.comprovarDatosCargados();
        if(archivoCargado){
          // Leer el archivo JSON  y emitir al aplicativo la carga del mismo.
          this.datosini.cargarJSON()
          .then((r:any)=>{
              this.localSt.guardarDatosStorage(r);
              this.localSt._EVENT_LOCALSTORAGE.emit('FILE_CARGADO');
          })
        }else{
          // Como el archivo ya  esta en local Storage, emitir que ya esta cargado.
          this.localSt._EVENT_LOCALSTORAGE.emit('FILE_CARGADO');
        }
  }

  ngOnInit() {

  }
}
