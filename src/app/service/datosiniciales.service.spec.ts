import { TestBed } from '@angular/core/testing';

import { DatosinicialesService } from './datosiniciales.service';

describe('DatosinicialesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DatosinicialesService = TestBed.get(DatosinicialesService);
    expect(service).toBeTruthy();
  });
});
