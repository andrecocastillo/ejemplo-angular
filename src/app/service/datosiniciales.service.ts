import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class DatosinicialesService {

  constructor(private http: HttpClient) { 
    
  }

  cargarJSON(){
    return new Promise(resolve => {
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
        let params: URLSearchParams = new URLSearchParams();
        this.http.get('./assets/tareas.json')
        .subscribe(data => {
          resolve(data);
       });
    });
  } 

}
