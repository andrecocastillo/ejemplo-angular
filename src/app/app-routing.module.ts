import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrincipalComponent } from './layout/principal/principal.component';
import { ListadoComponent } from './modulos/tareas/listado/listado.component';
import { NuevaComponent } from './modulos/tareas/nueva/nueva.component';

const routes: Routes = [
  { path: '',   redirectTo: '/tareas-listado', pathMatch: 'full' },
  {
    path: '',
    component: PrincipalComponent,
    children: [
        {
            path:'tareas-listado',
            component: ListadoComponent
        }
    ]
  },
  {
    path: 'tareas',
    component: PrincipalComponent,
    children: [
        {
            path:'nueva',
            component: NuevaComponent
        }
    ]
  }   
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
