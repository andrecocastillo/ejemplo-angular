import { Injectable } from '@angular/core';
import { EventEmitter } from 'events';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {

  public _EVENT_LOCALSTORAGE:EventEmitter = new EventEmitter();  

  constructor() { 

  }

  guardarDatosStorage(source){
    let datos = localStorage.getItem('tmpdb');
    if(!datos){
      localStorage.setItem('tmpdb', JSON.stringify(source));
    }
  }

  resetDatos(source){
      localStorage.setItem('tmpdb', JSON.stringify(source));
  }  

  comprovarDatosCargados(){
    let datos = localStorage.getItem('tmpdb');
    if(datos){
      return true;
    }else{
      return false;
    }
  }

  getDatos():Array<Object>{
    let datos:Array<Object> = [];
    let info:any = localStorage.getItem('tmpdb');
    datos = JSON.parse(info);
    return datos;
  }

  eliminarDato(idTarea:number){
    let datos = this.getDatos();
    datos = datos.filter(item => item[0] !== idTarea);
    localStorage.setItem('tmpdb', JSON.stringify(datos));
  }

  guardarDato(texto:string,fecha:string){
    if(!texto){return false;}
    let datos = this.getDatos();
    let id = datos.length ++;
    datos.push([id,texto,fecha]);
    datos = datos.filter(item => item[0]);
    localStorage.setItem('tmpdb', JSON.stringify(datos));
  }  
}
