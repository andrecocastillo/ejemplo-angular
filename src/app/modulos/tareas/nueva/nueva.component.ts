import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LocalstorageService } from 'src/app/provider/localstorage.service';

@Component({
  selector: 'app-nueva',
  templateUrl: './nueva.component.html',
  styleUrls: ['./nueva.component.css']
})
export class NuevaComponent implements OnInit {


  formTarea: FormGroup;
  erroText:string = "";
  okText:string = "";

  constructor(private formBuilder: FormBuilder,private localSt:LocalstorageService) { 
    this.formTarea = this.formBuilder.group({
      tarea: ['', Validators.required],
      fecha: ['', Validators.required]
    });
  }

  ngOnInit() { 

  }

  onSubmit(){
    if (this.formTarea.valid) {
        // Console guardar
        let tarea = this.formTarea.value.tarea;
        let fecha = this.formTarea.value.fecha;
        this.localSt.guardarDato(tarea,fecha);
        this.okText = 'Tarea guardada';
        this.erroText = '';

        this.formTarea.reset();
    }else{
      this.erroText = 'Datos incompletos';
      this.okText = '';
    }
  }
}
