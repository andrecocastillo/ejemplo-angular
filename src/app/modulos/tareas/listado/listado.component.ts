import { Component, OnInit } from '@angular/core';
import { LocalstorageService } from 'src/app/provider/localstorage.service';


@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  tareas:Array<Object>= [];

  constructor(private localSt:LocalstorageService) { 
    // Inciar procesos asegurandose que  el archivo ta  esta cargado
    localSt._EVENT_LOCALSTORAGE.on('FILE_CARGADO',item => {
        this.tareas = this.localSt.getDatos();
    });
  }

  ngOnInit() {
    this.tareas = this.localSt.getDatos();
  }

  eliminarItem(id:number){
    this.localSt.eliminarDato(id);
    this.tareas = this.localSt.getDatos();
  }
}
