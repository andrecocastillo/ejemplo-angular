import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrincipalComponent } from './layout/principal/principal.component';
import { ListadoComponent } from './modulos/tareas/listado/listado.component';
import { HeaderComponent } from './layout/comunes/header/header.component';
import { FooterComponent } from './layout/comunes/footer/footer.component';
import { NuevaComponent } from './modulos/tareas/nueva/nueva.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AppComponent,
    PrincipalComponent,
    ListadoComponent,
    HeaderComponent,
    FooterComponent,
    NuevaComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
